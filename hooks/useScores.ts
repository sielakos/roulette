import { useState, useCallback, useEffect } from "react";
import { useRecoilState, atom } from "recoil";

const scoreKey = "current-score";
const historyKey = "old-scores";

const scoreAtom = atom<number>({
  key: scoreKey,
  default: 100,
});

const historyAtom = atom<number[]>({
  key: historyKey,
  default: [],
});

function getScore(): number {
  const saved = sessionStorage.getItem(scoreKey);

  if (saved) {
    return parseInt(saved);
  }

  return 100;
}

function getHistory(): number[] {
  const saved = sessionStorage.getItem(historyKey);

  if (saved) {
    return JSON.parse(saved);
  }

  return [];
}

export function useScores() {
  const [score, setScore] = useRecoilState(scoreAtom);
  const [history, setHistory] = useRecoilState(historyAtom);

  const onNextScore = useCallback(
    (newScore: number) => {
      if (newScore === 0) {
        // game over new game
        const newHistory =
          history.length >= 10 ? history.slice(0, 9) : [...history];

        sessionStorage.setItem(scoreKey, "100");
        setScore(100);

        newHistory.unshift(score);

        sessionStorage.setItem(historyKey, JSON.stringify(newHistory));
        setHistory(newHistory);

        return;
      }

      setScore(newScore);
      sessionStorage.setItem(scoreKey, newScore.toString());
    },
    [history, score, setScore, setHistory]
  );

  useEffect(() => {
    setScore(getScore());
    setHistory(getHistory());
  }, [setScore, setHistory]);

  return {
    score,
    history,
    onNextScore,
  };
}
