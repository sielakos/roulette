import {
  useEffect,
  useCallback,
  useRef,
  useState,
  MutableRefObject,
} from "react";

export interface StopEvent<T extends HTMLElement> {
  name: string;
  start: (name: string) => void;
  ref: MutableRefObject<HTMLElement | null>;
}

export function useAnimation<T extends HTMLElement>(
  stopListener: (event: StopEvent<T>) => void = () => {}
) {
  const [active, setActive] = useState(false);
  const [stopFlag, setStopFlag] = useState(false);
  const [name, setName] = useState<string>();
  const ref: MutableRefObject<T | null> = useRef();
  const start = useCallback(
    (name: string) => {
      ref.current.classList.add(name);

      setName(name);
      setActive(true);
      setStopFlag(false);
    },
    [ref, setName, setActive, setStopFlag]
  );
  const stop = useCallback(() => {
    setStopFlag(true);
  }, [setStopFlag]);

  useEffect(() => {
    if (ref.current) {
      const startListener = () => {
        setActive(true);
      };
      const endListener = () => {
        ref.current.classList.remove(name);

        setActive(false);
        stopListener({
          name,
          start,
          ref,
        });
      };
      const iterationListener = () => {
        if (stopFlag) {
          endListener();
        }
      };

      ref.current.addEventListener("animationstart", startListener);
      ref.current.addEventListener("animationend", endListener);
      ref.current.addEventListener("animationiteration", iterationListener);

      return () => {
        if (ref.current) {
          ref.current.removeEventListener("animationstart", startListener);
          ref.current.removeEventListener("animationend", endListener);
          ref.current.removeEventListener(
            "animationiteration",
            iterationListener
          );
        }
      };
    }
  }, [ref.current, name, stopFlag, setActive, start]);

  return {
    ref,
    start,
    stop,
    active,
  };
}
