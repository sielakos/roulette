import { useCallback, useState } from "react";

import { Data } from "@/types/results";

import { useAnimation } from "./useAnimation";
import { useScores } from "./useScores";

interface Config {
  rotateWheelAnim: string;
  rotateBallAnim: string;
  toAngleAnim: string;
  resultClass: string;
}

async function getResult(): Promise<Data> {
  const response = await fetch("/api/roulette");
  const data: Data = await response.json();

  return data;
}

export function useRoulette({
  rotateWheelAnim,
  rotateBallAnim,
  toAngleAnim,
  resultClass,
}: Config) {
  const { score, onNextScore } = useScores();
  const [result, setResult] = useState<Data>();
  const [currentBet, setCurrentBet] = useState<"red" | "black">();

  const {
    ref: wheelRef,
    start: startWheel,
    stop: stopWheel,
  } = useAnimation<HTMLDivElement>();
  const {
    ref: ballRef,
    start: startBall,
    stop: stopBall,
    active,
  } = useAnimation<HTMLDivElement>(({ start, ref, name }) => {
    if (result && ref.current && name === rotateBallAnim) {
      const fieldAngleWidth = 360 / result.fieldCount;
      const angle = Math.round(fieldAngleWidth * result.index);

      ref.current.style.setProperty("--target", `${angle}deg`);
      start(toAngleAnim);
    } else if (name === toAngleAnim) {
      ref.current.classList.add(resultClass);
      onNextScore(result.color === currentBet ? score * 2 : 0);
    }
  });

  const bet = useCallback(
    async (type: "red" | "black") => {
      setCurrentBet(type);
      ballRef.current.classList.remove(resultClass);

      startWheel(rotateWheelAnim);
      startBall(rotateBallAnim);
      const result = await getResult();

      setResult(result);

      stopWheel();
      stopBall();
    },
    [startWheel, startBall, setCurrentBet]
  );

  return {
    wheelRef,
    ballRef,
    score,
    result,
    active,
    bet,
    currentBet,
  };
}
