import React from "react";

import styles from "@/styles/scoreboard.module.scss";
import { useScores } from "@/hooks/useScores";

export function ScoreBoard() {
  const { score, history } = useScores();

  return (
    <div className={styles.container}>
      <div className={styles.currentScore}>Current score is {score}</div>
      {history.map((oldScore) => (
        <div>{oldScore}</div>
      ))}
    </div>
  );
}
