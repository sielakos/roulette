import type { NextApiRequest, NextApiResponse } from "next";

import { Color, Data } from "@/types/results";

export const values = [
  0,
  32,
  15,
  19,
  4,
  21,
  2,
  25,
  17,
  34,
  6,
  27,
  13,
  36,
  11,
  30,
  8,
  32,
  10,
  5,
  24,
  16,
  33,
  1,
  20,
  14,
  31,
  9,
  22,
  18,
  29,
  7,
  28,
  12,
  35,
  3,
  26,
];

function getResult(): Data {
  const index = Math.floor(Math.random() * values.length);
  const colors: Color[] = ["black", "red"];
  const color: Color = index === 0 ? "green" : colors[index % 2];

  return { value: values[index], index, color, fieldCount: values.length };
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  res.status(200).json(getResult());
}
