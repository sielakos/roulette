import React from "react";
import Image from "next/image";

import styles from "@/styles/index.module.scss";
import { useRoulette } from "@/hooks/useRoulette";
import { ScoreBoard } from "@/components/ScoreBoard";

export default function Index() {
  const { ballRef, wheelRef, bet, result, score, active } = useRoulette({
    rotateBallAnim: styles.rotateBall,
    rotateWheelAnim: styles.rotate,
    toAngleAnim: styles.toAngle,
    resultClass: styles.result,
  });

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Welcome to roulette</h1>
      {result && (
        <div
          className={styles.resultText}
          style={{ display: active ? "none" : "block" }}
        >
          Winning number is {result.value} {result.color}
        </div>
      )}
      <div className={styles.rouletteContainer}>
        <div ref={wheelRef}>
          <Image
            className={styles.roulette}
            src="/roulette.png"
            alt="roulette"
            width="400"
            height="400"
          />
          <div ref={ballRef} className={styles.ballContainer}>
            <div className={styles.ball} />
          </div>
        </div>
      </div>
      <button
        className={styles.red}
        onClick={() => bet("red")}
        disabled={active}
      >
        Bet on red
      </button>
      <button
        className={styles.black}
        onClick={() => bet("black")}
        disabled={active}
      >
        Bet on black
      </button>
      <div className={styles.scoreboard}>
        <ScoreBoard />
      </div>
    </div>
  );
}
