import App from "next/app";
import { RecoilRoot } from "recoil";
import "@/styles/globals.scss";

export default function MyApp({ Component, pageProps }) {
  return (
    <RecoilRoot>
      <Component {...pageProps} />
    </RecoilRoot>
  );
}
