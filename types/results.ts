export type Color = "green" | "red" | "black";

export interface Data {
  value: number;
  index: number;
  color: Color;
  fieldCount: number;
}
